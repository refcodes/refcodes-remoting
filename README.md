# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact is a lightweight `RPC` ([remote procedure call](http://en.wikipedia.org/wiki/Remote_procedure_call)) toolkit (you may also call it an `IPC` ([inter process communication](http://en.wikipedia.org/wiki/Inter-process_communication)) toolkit). On your server you publish any instance (any*** **Java** ***object), on your client you may access this instance via a proxy object which looks and feels exactly as the remote instance on the server.***

## Getting started ##

> Please refer to the [refcodes-remoting: Face-to-face lightweight remote method calls](https://www.metacodes.pro/refcodes/refcodes-remoting) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-remoting</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-remoting). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-remoting).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-remoting/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
