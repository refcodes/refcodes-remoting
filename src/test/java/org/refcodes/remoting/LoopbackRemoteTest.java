// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.refcodes.exception.VetoException;
import org.refcodes.io.LoopbackDatagramsTransceiver;
import org.refcodes.runtime.SystemProperty;

public class LoopbackRemoteTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int ITERARTIONS = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	@Test
	public void testLoopbackRemote() throws VetoException, IOException {
		final LoopbackDatagramsTransceiver<Serializable> theClientTransceiver = new LoopbackDatagramsTransceiver<>();
		final LoopbackDatagramsTransceiver<Serializable> theServerTransceiver = new LoopbackDatagramsTransceiver<>();
		theClientTransceiver.open( theServerTransceiver );
		theServerTransceiver.open( theClientTransceiver );
		final RemoteClient theClientRemote = new RemoteClient();
		theClientRemote.open( theClientTransceiver );
		final RemoteServer theServerRemote = new RemoteServer();
		theServerRemote.open( theServerTransceiver );
		final List<String> thePublishedList = new ArrayList<>();
		thePublishedList.add( "A" );
		thePublishedList.add( "B" );
		thePublishedList.add( "C" );
		Iterator<String> e = thePublishedList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		theServerRemote.publishSubject( thePublishedList );
		synchronized ( this ) {
			try {
				wait( 500 );
			}
			catch ( InterruptedException ie ) {}
		}
		final Object theProxy = theClientRemote.proxies().next();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Proxy class = " + theProxy.getClass() );
		}
		if ( theProxy instanceof List ) {
			final List<String> theProxyList = (List<String>) theProxy;
			theProxyList.add( "1" );
			theProxyList.add( "2" );
			theProxyList.add( "3" );
			synchronized ( LoopbackRemoteTest.this ) {
				LoopbackRemoteTest.this.notifyAll();
			}
		}
		e = thePublishedList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( thePublishedList.contains( "1" ) );
		assertTrue( thePublishedList.contains( "2" ) );
		assertTrue( thePublishedList.contains( "3" ) );
		assertTrue( thePublishedList.contains( "A" ) );
		assertTrue( thePublishedList.contains( "B" ) );
		assertTrue( thePublishedList.contains( "C" ) );
		// theClientRemote.close();
		theServerRemote.close();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testTransceiverOpenClose() throws VetoException, IOException {
		final LoopbackDatagramsTransceiver<Serializable> theClientTransceiver = new LoopbackDatagramsTransceiver<>();
		final LoopbackDatagramsTransceiver<Serializable> theServerTransceiver = new LoopbackDatagramsTransceiver<>();
		theClientTransceiver.open( theServerTransceiver );
		theServerTransceiver.open( theClientTransceiver );
		final RemoteClient theClientRemote = new RemoteClient();
		theClientRemote.open( theClientTransceiver );
		final RemoteServer theServerRemote = new RemoteServer();
		theServerRemote.open( theServerTransceiver );
		final List<String> thePublishedList = new ArrayList<>();
		thePublishedList.add( "A" );
		thePublishedList.add( "B" );
		thePublishedList.add( "C" );
		Iterator<String> e = thePublishedList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Closing server transcveiver." );
		}
		theServerTransceiver.close();
		synchronized ( this ) {
			try {
				wait( 500 );
			}
			catch ( InterruptedException ie ) {}
		}
		theServerTransceiver.open( theClientTransceiver );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Opening server transcveiver." );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Closing server transcveiver." );
		}
		theServerTransceiver.close();
		theServerTransceiver.open( theClientTransceiver );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Opening server transcveiver." );
		}
		theServerRemote.publishSubject( thePublishedList );
		synchronized ( this ) {
			try {
				wait( 500 );
			}
			catch ( InterruptedException ie ) {}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Closing client transcveiver." );
		}
		theClientTransceiver.close();
		synchronized ( this ) {
			try {
				wait( 500 );
			}
			catch ( InterruptedException ie ) {}
		}
		theClientTransceiver.open( theServerTransceiver );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Opening client transcveiver." );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Closing client transcveiver." );
		}
		theClientTransceiver.close();
		theClientTransceiver.open( theServerTransceiver );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Opening client transcveiver." );
		}
		final Object theProxy = theClientRemote.proxies().next();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Proxy class = " + theProxy.getClass() );
		}
		if ( theProxy instanceof List ) {
			final List<String> theProxyList = (List<String>) theProxy;
			theProxyList.add( "1" );
			theProxyList.add( "2" );
			theProxyList.add( "3" );
			synchronized ( LoopbackRemoteTest.this ) {
				LoopbackRemoteTest.this.notifyAll();
			}
		}
		e = thePublishedList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( thePublishedList.contains( "1" ) );
		assertTrue( thePublishedList.contains( "2" ) );
		assertTrue( thePublishedList.contains( "3" ) );
		assertTrue( thePublishedList.contains( "A" ) );
		assertTrue( thePublishedList.contains( "B" ) );
		assertTrue( thePublishedList.contains( "C" ) );
		// theClientRemote.close();
		theServerRemote.close();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testStressLoopbackRemote() throws IOException {
		final LoopbackDatagramsTransceiver<Serializable> theClientTransceiver = new LoopbackDatagramsTransceiver<>();
		final LoopbackDatagramsTransceiver<Serializable> theServerTransceiver = new LoopbackDatagramsTransceiver<>();
		theClientTransceiver.open( theServerTransceiver );
		theServerTransceiver.open( theClientTransceiver );
		final RemoteClient theClientRemote = new RemoteClient();
		theClientRemote.open( theClientTransceiver );
		final RemoteServer theServerRemote = new RemoteServer();
		theServerRemote.open( theServerTransceiver );
		final CountingList<String> theList = new CountingList<>();
		theList.add( "A" );
		theList.add( "B" );
		theList.add( "C" );
		assertEquals( 3, theList.getAddCount() );
		Iterator<String> e = theList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		theServerRemote.publishSubject( theList );
		synchronized ( this ) {
			try {
				wait( 500 );
			}
			catch ( InterruptedException ie ) {}
		}
		final Object theProxy = theClientRemote.proxies().next();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Proxy class = " + theProxy.getClass() );
		}
		if ( theProxy instanceof List ) {
			final List<String> theProxyList = (List<String>) theProxy;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Perforimg ADD operations:" );
			}
			for ( int i = 0; i < ITERARTIONS; i++ ) {
				if ( i % 10 == 0 ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "ADD(" + i + ")" );
					}
				}
				theProxyList.add( Integer.toString( i ) );
				assertTrue( theProxyList.contains( Integer.toString( i ) ) );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Perforimg REMOVE operations:" );
			}
			for ( int i = 0; i < ITERARTIONS; i++ ) {
				if ( i % 10 == 0 ) {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "REMOVE(" + i + ")" );
					}
				}
				theProxyList.remove( Integer.toString( i ) );
				assertFalse( theProxyList.contains( Integer.toString( i ) ) );
			}
			assertFalse( theProxyList.isEmpty() );
			assertTrue( theProxyList.contains( "A" ) );
			assertTrue( theProxyList.contains( "B" ) );
			assertTrue( theProxyList.contains( "C" ) );
			theProxyList.clear();
			assertTrue( theProxyList.isEmpty() );
			synchronized ( LoopbackRemoteTest.this ) {
				LoopbackRemoteTest.this.notifyAll();
			}
		}
		e = theList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( theList.isEmpty() );
		assertEquals( ITERARTIONS + 3, theList.getAddCount() );
		assertEquals( ITERARTIONS, theList.getRemoveCount() );
		assertEquals( 1, theList.getClearCount() );
		theClientRemote.close();
		theServerRemote.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public class CountingList<T> extends ArrayList<T> implements List<T>, Serializable {
		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private int _addCount = 0;
		private int _removeCount = 0;
		private int _clearCount = 0;

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		@Override
		public boolean add( T o ) {
			final boolean isAdd = super.add( o );
			_addCount++;
			return isAdd;
		}

		@Override
		public void clear() {
			super.clear();
			_clearCount++;
		}

		@Override
		public boolean remove( Object o ) {
			final boolean isRemove = super.remove( o );
			_removeCount++;
			return isRemove;
		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		public int getAddCount() {
			return _addCount;
		}

		public int getRemoveCount() {
			return _removeCount;
		}

		public int getClearCount() {
			return _clearCount;
		}
	}
}
