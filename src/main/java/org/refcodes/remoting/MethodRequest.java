// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The Interface MethodRequest.
 */
public interface MethodRequest extends Session {

	/**
	 * Returns the name of the method represented by this method object, as a
	 * String.
	 * 
	 * @return The name of the method represented by this method object is
	 *         returned
	 */
	String getMethodName();

	/**
	 * Returns an array of Class objects that represent the formal parameter
	 * types, in declaration order, of the method represented by this Method
	 * object.
	 * 
	 * @return Description is currently not available!
	 */
	Class<?>[] getArgumentTypeArray();

	// Class getReturnType();

	/**
	 * Returns the arguments to be passed to the method which in turn will be
	 * invoked on the target object.
	 * 
	 * @return The Object array being the arguments to be passed to the method
	 *         of the target.
	 */
	Object[] getArgumentArray();
}
