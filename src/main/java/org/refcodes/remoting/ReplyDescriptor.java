// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;

import org.refcodes.mixin.Clearable;

/**
 * The Class ReplyDescriptorImpl.
 */
class ReplyDescriptor implements Reply, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _hasReply = false;
	private String _instanceId = null;
	private Object _returnValue = null;
	private String _sessionId = null;
	private Throwable _throwable = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link Reply} instance.
	 */
	public ReplyDescriptor() {}

	/**
	 * Creates a {@link Reply} instance.
	 * 
	 * @param aReturnValue The return value; if any regular return then no
	 *        exception must be set.
	 * @param aException The exception in case an exception occurred (then there
	 *        must not be a return value).
	 * @param aMethodRequestDescriptor The {@link MethodRequest} describing the
	 *        according method request.
	 */
	public ReplyDescriptor( Object aReturnValue, Throwable aException, MethodRequest aMethodRequestDescriptor ) {
		this( aReturnValue, aException, aMethodRequestDescriptor.getInstanceId(), aMethodRequestDescriptor.getSessionId() );
	}

	/**
	 * Creates a {@link Reply} instance.
	 * 
	 * @param aReturnValue The return value; if any regular return then no
	 *        exception must be set.
	 * @param aException The exception in case an exception occurred (then there
	 *        must not be a return value).
	 * @param aInstanceId The instance ID in question.
	 * @param aSessionId The session ID in question.
	 */
	public ReplyDescriptor( Object aReturnValue, Throwable aException, String aInstanceId, String aSessionId ) {
		_instanceId = aInstanceId;
		_sessionId = aSessionId;
		_throwable = aException;
		_returnValue = aReturnValue;
		_hasReply = true;
	}

	/**
	 * Creates a {@link Reply} instance.
	 *
	 * @param instanceId Description is currently not available!
	 * @param sessionId Description is currently not available!
	 */
	public ReplyDescriptor( String instanceId, String sessionId ) {
		_returnValue = null;
		_throwable = null;
		_sessionId = sessionId;
		_instanceId = instanceId;
		_hasReply = false;
	}

	/**
	 * Creates a {@link Reply} instance.
	 * 
	 * @param sessionId Description is currently not available!
	 */
	protected ReplyDescriptor( String sessionId ) {
		_sessionId = sessionId;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {
		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getReturnValue() {
		return _returnValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSessionId() {
		return _sessionId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Throwable getException() {
		return _throwable;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasReply() {
		return _hasReply;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isReturnValue() {
		return ( !isException() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isException() {
		return ( _throwable != null );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReply( Reply aReply ) {
		if ( aReply == null ) {
			_returnValue = null;
			_throwable = null;
			_hasReply = false;
		}
		else {
			if ( !aReply.getInstanceId().equals( getInstanceId() ) ) {
				throw new IllegalArgumentException( "Wrong instance ID of  method reply argument." );
			}
			if ( !aReply.getSessionId().equals( getSessionId() ) ) {
				throw new IllegalArgumentException( "Wrong session ID of method reply argument." );
			}
			_returnValue = aReply.getReturnValue();
			_throwable = aReply.getException();
			_hasReply = true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReturnValue( Object returnValue ) {
		_returnValue = returnValue;
		if ( _returnValue != null ) {
			_returnValue = SerializeUtility.toSerializable( _returnValue );
		}
		_hasReply = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setException( Throwable aException ) {
		_throwable = aException;
		_hasReply = true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the checks for reply.
	 *
	 * @param hasReply Description is currently not available!
	 */
	protected void setHasReply( boolean hasReply ) {
		_hasReply = hasReply;
	}

	/**
	 * Sets the instance id.
	 *
	 * @param instanceId Description is currently not available!
	 */
	protected void setInstanceId( String instanceId ) {
		_instanceId = instanceId;
	}

	/**
	 * Sets the method reply descriptor.
	 *
	 * @param methodReplyDescriptor Description is currently not available!
	 */
	protected void setMethodReplyDescriptor( Reply methodReplyDescriptor ) {
		_instanceId = methodReplyDescriptor.getInstanceId();
		_sessionId = methodReplyDescriptor.getSessionId();
		_returnValue = methodReplyDescriptor.getReturnValue();
		_throwable = methodReplyDescriptor.getException();
		_hasReply = true;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId Description is currently not available!
	 */
	protected void setSessionId( String sessionId ) {
		_sessionId = sessionId;
	}

	/**
	 * Same as {@link Clearable#clear()}.
	 */
	protected void clear() {
		_instanceId = null;
		_returnValue = null;
		_sessionId = null;
		_throwable = null;
		_hasReply = false;
	}
}
