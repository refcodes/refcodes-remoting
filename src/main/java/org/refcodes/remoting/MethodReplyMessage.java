// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The {@link MethodReplyMessage} describes a method's result.
 */
class MethodReplyMessage extends ReplyDescriptor implements ServerMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new {@link MethodReplyMessage} instance.
	 */
	public MethodReplyMessage() {}

	/**
	 * Creates a new {@link MethodReplyMessage} instance.
	 * 
	 * @param aReplyDescriptor The {@link Reply} for the method reply in
	 *        question.
	 */
	public MethodReplyMessage( Reply aReplyDescriptor ) {
		this( aReplyDescriptor.getReturnValue(), aReplyDescriptor.getException(), aReplyDescriptor.getSessionId(), aReplyDescriptor.getInstanceId() );
	}

	/**
	 * Instantiates a new method reply aMessage impl.
	 *
	 * @param aReturnValue the return value
	 * @param aException the exception
	 * @param aMethodRequestDescriptor the method request descriptor
	 */
	public MethodReplyMessage( Object aReturnValue, Throwable aException, MethodRequest aMethodRequestDescriptor ) {
		super( aReturnValue, aException, aMethodRequestDescriptor );
	}

	/**
	 * Instantiates a new method reply aMessage impl.
	 *
	 * @param returnValue the return value
	 * @param throwable the throwable
	 * @param instanceId the instance id
	 * @param sessionId the session id
	 */
	public MethodReplyMessage( Object returnValue, Throwable throwable, String instanceId, String sessionId ) {
		super( returnValue, throwable, instanceId, sessionId );
	}

	/**
	 * Instantiates a new method reply aMessage impl.
	 *
	 * @param instanceId the instance id
	 * @param sessionId the session id
	 */
	public MethodReplyMessage( String instanceId, String sessionId ) {
		super( instanceId, sessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReply( Reply methodReply ) {
		super.setReply( methodReply );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReturnValue( Object returnValue ) {
		super.setReturnValue( returnValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setException( Throwable throwable ) {
		super.setException( throwable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSessionId( String sessionId ) {
		super.setSessionId( sessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMethodReplyDescriptor( Reply methodReplyDescriptor ) {
		super.setMethodReplyDescriptor( methodReplyDescriptor );
	}
}
