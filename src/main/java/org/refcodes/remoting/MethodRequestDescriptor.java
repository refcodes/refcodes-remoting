// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;
import java.lang.reflect.Method;

import org.refcodes.mixin.Clearable;

/**
 * The {@link MethodRequestDescriptor} describes a request.
 */
class MethodRequestDescriptor implements MethodRequest, Serializable {

	private static final long serialVersionUID = 1L;

	private Object[] _argumentArray;

	private String _instanceId;

	private String _methodName;

	private Class<?>[] _parameterTypes;

	private String _sessionId;

	/**
	 * Creates a new MethodRequestDescriptor object.
	 */
	public MethodRequestDescriptor() {}

	/**
	 * Creates a new MethodRequestDescriptor object.
	 * 
	 * @param aMethodRequestDescriptor The {@link MethodRequest} in question.
	 */
	public MethodRequestDescriptor( MethodRequest aMethodRequestDescriptor ) {
		_instanceId = aMethodRequestDescriptor.getInstanceId();
		_sessionId = aMethodRequestDescriptor.getSessionId();
		_argumentArray = aMethodRequestDescriptor.getArgumentArray();
		_methodName = aMethodRequestDescriptor.getMethodName();
		_parameterTypes = aMethodRequestDescriptor.getArgumentTypeArray();
	}

	/**
	 * Creates a new MethodRequestDescriptor object.
	 * 
	 * @param aMethod The method in question.
	 * @param aArguments The arguments to be used for invoking the method.
	 * @param aInstanceId The instance ID of the request.
	 * @param aSessionId The session ID for this request.
	 */
	public MethodRequestDescriptor( Method aMethod, Object[] aArguments, String aInstanceId, String aSessionId ) {
		_instanceId = aInstanceId;
		_sessionId = aSessionId;
		_argumentArray = aArguments;
		_methodName = aMethod.getName();
		_parameterTypes = aMethod.getParameterTypes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {

		if ( obj instanceof Method method ) {
			if ( !_methodName.equals( method.getName() ) ) {
				return false;
			}

			if ( _parameterTypes.length != method.getParameterTypes().length ) {
				return false;
			}

			final int theLength = _parameterTypes.length;
			boolean isMatch;
			for ( int i = 0; i < theLength; i++ ) {
				isMatch = false;
				for ( int l = 0; l < theLength; l++ ) {
					if ( _parameterTypes[i].equals( method.getParameterTypes()[l] ) ) {
						isMatch = true;
						break;
					}
				}
				if ( !isMatch ) {
					return false;
				}
			}
			return true;
		}
		else if ( obj instanceof MethodRequest ) {
			final MethodRequest method = (MethodRequest) obj;
			if ( !_methodName.equals( method.getMethodName() ) ) {
				return false;
			}
			if ( _parameterTypes.length != method.getArgumentTypeArray().length ) {
				return false;
			}

			final int theLength = _parameterTypes.length;
			boolean isMatch;
			for ( int i = 0; i < theLength; i++ ) {
				isMatch = false;
				for ( int l = 0; l < theLength; l++ ) {
					if ( _parameterTypes[i].equals( method.getArgumentTypeArray()[l] ) ) {
						isMatch = true;

						break;
					}
				}
				if ( !isMatch ) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getArgumentArray() {

		return _argumentArray;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?>[] getArgumentTypeArray() {
		/*
		 * Class<?>[] parameterTypes = new Class<?>[_parameterTypes.length]; for
		 * (int i = 0; i < _parameterTypes.length; i++) {if
		 * (_parameterTypes[i].equals(Boolean.class))parameterTypes[i] =
		 * Boolean.TYPE;elseparameterTypes[i] = _parameterTypes[i];}return
		 * parameterTypes;
		 */
		return _parameterTypes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {

		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMethodName() {

		return _methodName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSessionId() {

		return _sessionId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		String out = super.toString() + "\n" + "instance id = " + _instanceId + "\n" + "session id = " + _sessionId + "\n" + "name = " + _methodName + "\n";
		if ( _argumentArray != null ) {
			for ( int i = 0; i < _argumentArray.length; i++ ) {
				out += "arguments[" + i + "] = " + _argumentArray[i] + "\n";
			}
		}
		return out;
	}

	/**
	 * Sets the argument array.
	 *
	 * @param arguments Description is currently not available!
	 */
	protected void setArgumentArray( Object[] arguments ) {
		_argumentArray = arguments;
		if ( _argumentArray != null ) {
			for ( int i = 0; i < _argumentArray.length; i++ ) {
				_argumentArray[i] = SerializeUtility.toSerializable( _argumentArray[i] );
			}
		}
	}

	/**
	 * Sets the instance id.
	 *
	 * @param instanceId Description is currently not available!
	 */
	protected void setInstanceId( String instanceId ) {
		_instanceId = instanceId;
	}

	/**
	 * Sets the method name.
	 *
	 * @param aMethodName Description is currently not available!
	 */
	protected void setMethodName( String aMethodName ) {
		_methodName = aMethodName;
	}

	/**
	 * Sets the method request descriptor.
	 *
	 * @param methodRequestDescriptor Description is currently not available!
	 */
	protected void setMethodRequestDescriptor( MethodRequest methodRequestDescriptor ) {
		_instanceId = methodRequestDescriptor.getInstanceId();
		_methodName = methodRequestDescriptor.getMethodName();
		_argumentArray = methodRequestDescriptor.getArgumentArray();
		_parameterTypes = methodRequestDescriptor.getArgumentTypeArray();
		_sessionId = methodRequestDescriptor.getSessionId();
	}

	/**
	 * Sets the parameter types.
	 *
	 * @param parameterTypes Description is currently not available!
	 */
	protected void setParameterTypes( Class<?>[] parameterTypes ) {
		_parameterTypes = parameterTypes;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId Description is currently not available!
	 */
	protected void setSessionId( String sessionId ) {
		_sessionId = sessionId;
	}

	/**
	 * Same as {@link Clearable#clear()}.
	 */
	protected void clear() {
		_instanceId = null;
		_parameterTypes = null;
		_sessionId = null;
		_argumentArray = null;
		_methodName = null;
	}
}
