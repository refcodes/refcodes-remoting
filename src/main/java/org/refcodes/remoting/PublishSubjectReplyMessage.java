// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The {@link PublishSubjectReplyMessage} describes a reply of a publish
 * operation.
 */
class PublishSubjectReplyMessage extends ReplyDescriptor implements ClientMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new {@link PublishSubjectReplyMessage} instance with the
	 * {@link RemotingConsts#STATIC_SESS_ID} session ID.
	 */
	public PublishSubjectReplyMessage() {
		super( AbstractRemote.STATIC_SESSION_ID );
	}

	/**
	 * Creates a new {@link PublishSubjectReplyMessage} instance.
	 * 
	 * @param aReplyDescriptor The {@link Reply} describing the reply.
	 */
	public PublishSubjectReplyMessage( Reply aReplyDescriptor ) {
		this( aReplyDescriptor.getReturnValue(), aReplyDescriptor.getException(), aReplyDescriptor.getInstanceId() );
	}

	/**
	 * Instantiates a new publish subject reply aMessage impl.
	 *
	 * @param aReturnValue the return value
	 * @param aException the exception
	 * @param aMethodRequestDescriptor the method request descriptor
	 */
	public PublishSubjectReplyMessage( Object aReturnValue, Throwable aException, MethodRequest aMethodRequestDescriptor ) {
		super( aReturnValue, aException, aMethodRequestDescriptor );
	}

	/**
	 * Creates a new {@link PublishSubjectReplyMessage} instance with the
	 * {@link RemotingConsts#STATIC_SESSION_ID} session ID.
	 * 
	 * @param aReturnValue The return value; if any regular return then no
	 *        exception must be set.
	 * @param aException The exception in case an exception occurred (then there
	 *        must not be a return value).
	 * @param aInstanceId The instance ID in question.
	 */
	public PublishSubjectReplyMessage( Object aReturnValue, Throwable aException, String aInstanceId ) {
		super( aReturnValue, aException, AbstractRemote.STATIC_SESSION_ID, aInstanceId );
	}

	/**
	 * Creates a new {@link PublishSubjectReplyMessage} instance with the
	 * {@link RemotingConsts#STATIC_SESS_ID} session ID.
	 * 
	 * @param aInstanceId The instance ID in question.
	 */
	public PublishSubjectReplyMessage( String aInstanceId ) {
		super( AbstractRemote.STATIC_SESSION_ID, aInstanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		setInstanceId( null );
		setReply( null );
	}

	/**
	 * Sets the exception.
	 *
	 * @param throwable Description is currently not available!
	 */
	public void setException( Exception throwable ) {
		super.setException( throwable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHasReply( boolean hasReply ) {
		super.setHasReply( hasReply );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSessionId( String sessionId ) {
		super.setSessionId( sessionId );
	}
}
