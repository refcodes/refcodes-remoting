// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;
import java.util.Iterator;
import java.util.ListIterator;

import org.refcodes.io.SerializableIterator;
import org.refcodes.io.SerializableListIterator;

/**
 * Utility providing useful serialization methods.
 */
public final class SerializeUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private SerializeUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts instances of common non-serializable types such as
	 * {@link Iterator} and {@link ListIterator} into serializable instances.
	 * Testing for the {@link Serializable} interface is not sufficient to
	 * determine whether an instance is serializable as of the
	 * https://www.metacodes.pro handling of the {@link Serializable} tag
	 * interface in the JVM. Though in case the type of the instance is not
	 * tagged with the {@link Serializable} interface, then <code>null</code> is
	 * returned. In case an instance's type provided is not supported by this
	 * method, then the instance is returned as is; it might already be
	 * serializable.
	 * 
	 * @param aObject The object to be turned into a serializable counterpart.
	 *        In case it is not identified or supported (it might already be
	 *        fully serializable), then the instance is returned unmodified.
	 * 
	 * @return The serializable counterpart of the provided instance in case the
	 *         type is supported by this method or the instance in case it is
	 *         {@link Serializable}. In case the type of the instance is not
	 *         tagged with the {@link Serializable} interface, then
	 *         <code>null</code> is returned.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Serializable toSerializable( Object aObject ) {
		if ( ( aObject instanceof ListIterator ) && ( !( aObject instanceof SerializableListIterator ) ) ) {
			return new SerializableListIterator( (ListIterator) aObject );
		}
		if ( ( aObject instanceof Iterator ) && ( !( aObject instanceof SerializableIterator ) ) ) {
			return new SerializableIterator( (Iterator) aObject );
		}
		return !( aObject instanceof Serializable ) ? null : (Serializable) aObject;
	}
}
