// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.remoting;

/**
 * Provides an accessor for a proxy property.
 */
public interface ProxyAccessor {

	/**
	 * Retrieves the proxy from the proxy property.
	 * 
	 * @param <P> The expected type of the proxy.
	 * 
	 * @return The proxy stored by the proxy property.
	 */
	<P> P getProxy();

	/**
	 * Provides a mutator for a proxy property.
	 */
	public interface ProxyMutator {

		/**
		 * Sets the proxy for the proxy property.
		 * 
		 * @param <P> The expected type of the proxy.
		 * @param aProxy The proxy to be stored by the proxy property.
		 */
		<P> void setProxy( P aProxy );
	}

	/**
	 * Provides a builder method for the according property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ProxyBuilder<B extends ProxyBuilder<B>> {

		/**
		 * Sets the value for the proxy property.
		 * 
		 * @param <P> The expected type of the proxy.
		 * @param aProxy The value to be stored by the proxy property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		<P> B withProxy( P aProxy );
	}

	/**
	 * Provides a proxy property.
	 */
	public interface ProxyProperty extends ProxyAccessor, ProxyMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setProxy(Object)} and returns the very same value (getter).
		 * 
		 * @param <P> The expected type of the proxy.
		 * @param aProxy The value to set (via {@link #setProxy(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default <P> P letProxy( P aProxy ) {
			setProxy( aProxy );
			return aProxy;
		}
	}
}
