// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * Provides an accessor for a session ID property.
 */
public interface SessionIdAccessor {

	/**
	 * Retrieves the sessionId from the session ID property.
	 * 
	 * @return The sessionId stored by the session ID property.
	 */
	String getSessionId();

	/**
	 * Provides a mutator for a session ID property.
	 */
	public interface SessionIdMutator {

		/**
		 * Sets the session ID for the sessionId property.
		 * 
		 * @param aSessionId The session ID to be stored by the session ID
		 *        property.
		 */
		void setSessionId( String aSessionId );
	}

	/**
	 * Provides a builder method for a the according property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SessionIdBuilder<B extends SessionIdBuilder<B>> {

		/**
		 * Sets the value for the sessionId property.
		 * 
		 * @param aSessionId The value to be stored by the sessionId property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSessionId( String aSessionId );
	}

	/**
	 * Provides a session ID property.
	 */
	public interface SessionIdProperty extends SessionIdAccessor, SessionIdMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setSessionId(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aSessionId The {@link String} to set (via
		 *        {@link #setSessionId(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letSessionId( String aSessionId ) {
			setSessionId( aSessionId );
			return aSessionId;
		}
	}
}
