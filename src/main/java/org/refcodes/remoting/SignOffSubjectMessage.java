// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import org.refcodes.mixin.ReadTimeoutMillisAccessor;
import org.refcodes.mixin.ReadTimeoutMillisAccessor.ReadTimeoutMillisProperty;

/**
 * The {@link SignOffSubjectMessage} describes signing of a subject..
 */
class SignOffSubjectMessage extends InstanceDescriptor implements ServerMessage, ReadTimeoutMillisAccessor, ReadTimeoutMillisProperty {

	private static final long serialVersionUID = 1L;

	private long _timeoutInMs;

	/**
	 * {@inheritDoc}.
	 */
	public SignOffSubjectMessage() {}

	/**
	 * Instantiates a new signoff subject aMessage impl.
	 *
	 * @param aInstanceDescriptor the instance descriptor
	 */
	public SignOffSubjectMessage( InstanceId aInstanceDescriptor ) {
		super( aInstanceDescriptor.getInstanceId() );
	}

	/**
	 * Instantiates a new signoff subject aMessage impl.
	 *
	 * @param aInstanceId the instance id
	 */
	public SignOffSubjectMessage( String aInstanceId ) {
		super( aInstanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getReadTimeoutMillis() {
		return _timeoutInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setReadTimeoutMillis( long aReadTimeoutInMs ) {
		_timeoutInMs = aReadTimeoutInMs;
	}
}
