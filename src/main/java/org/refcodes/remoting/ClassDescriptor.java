// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;

import org.refcodes.mixin.Clearable;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.runtime.Execution;

/**
 * The most common information to be provided when describing an object residing
 * in a {@link RemoteServer} and which is linked to a {@link RemoteClient}. The
 * {@link RemoteServer} creates this descriptor and passes it to the
 * {@link RemoteClient}, the {@link RemoteClient} attaches this information to a
 * {@link ProxyDescriptor} describing its proxy instances.
 */
@SuppressWarnings("rawtypes")
public class ClassDescriptor implements InstanceId, TypeAccessor, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Class<?> _type;
	private String _instanceId;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new ClassDescriptor object.
	 */
	public ClassDescriptor() {}

	/**
	 * Constructs a {@link ClassDescriptor} instance with the given attributes.
	 * 
	 * @param aClass The class for the subject in question.
	 * @param aInstanceId The instance ID of the subject in question.
	 */
	public ClassDescriptor( Class<?> aClass, String aInstanceId ) {
		_instanceId = aInstanceId;
		_type = aClass;
	}

	/**
	 * Constructs a {@link ClassDescriptor} instance from the provided
	 * {@link ClassDescriptor} instance.
	 * 
	 * @param aClassDescriptor The {@link ClassDescriptor} providing the
	 *        instance ID and the type.
	 */
	public ClassDescriptor( ClassDescriptor aClassDescriptor ) {
		_instanceId = aClassDescriptor.getInstanceId();
		_type = aClassDescriptor.getType();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {
		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getType() {
		return _type;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return Execution.toString( "Instance ID = <" + _instanceId + ">; class = " + _type.toString(), super.toString() );
	}

	/**
	 * Sets the class.
	 *
	 * @param type Description is currently not available!
	 */
	protected void setClass( Class<?> type ) {
		_type = type;
	}

	/**
	 * Sets the class descriptor.
	 *
	 * @param classDescriptor Description is currently not available!
	 */
	protected void setClassDescriptor( ClassDescriptor classDescriptor ) {
		_type = classDescriptor.getType();
		_instanceId = classDescriptor.getInstanceId();
	}

	/**
	 * Sets the instance id.
	 *
	 * @param instanceId Description is currently not available!
	 */
	protected void setInstanceId( String instanceId ) {
		_instanceId = instanceId;
	}

	/**
	 * Same as {@link Clearable#clear()}.
	 */
	protected void clear() {
		_instanceId = null;
		_type = null;
	}
}
