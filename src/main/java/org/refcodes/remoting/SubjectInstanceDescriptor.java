// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The Class SubjectInstanceDescriptor.
 */
class SubjectInstanceDescriptor extends SubjectDescriptor implements InstanceId {

	private static final long serialVersionUID = 1L;

	private final String _instanceId;

	/**
	 * Creates a new {@link SubjectInstance} instance.
	 * 
	 * @param aSubject The subject in question.
	 * @param aInstanceId The instance ID for the subject. available
	 */
	public SubjectInstanceDescriptor( Object aSubject, String aInstanceId ) {
		super( aSubject );
		_instanceId = aInstanceId;
	}

	/**
	 * Creates a new {@link SubjectInstance} instance.
	 * 
	 * @param aSubjectDescriptor The {@link SubjectDescriptor} describing the
	 *        subject.
	 * @param aInstanceId The instance ID for the subject. available
	 */
	public SubjectInstanceDescriptor( SubjectDescriptor aSubjectDescriptor, String aInstanceId ) {
		super( aSubjectDescriptor.getSubject() );
		_instanceId = aInstanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {

		return _instanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		final String out = super.toString() + "\n" + "instance id = " + _instanceId + "\n";

		return out;
	}
}
