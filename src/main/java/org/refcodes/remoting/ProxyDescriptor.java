// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import org.refcodes.runtime.Execution;

/**
 * The {@link ProxyDescriptor} describes a proxy instance and additional meta
 * data for a {@link RemoteClient} to link the proxy in question with the
 * according subject residing in a {@link RemoteServer}.
 */
class ProxyDescriptor extends ClassDescriptor implements ProxyAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;
	private Object _proxy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new ProxyDescriptor object.
	 */
	public ProxyDescriptor() {}

	/**
	 * Creates a new {@link ProxyDescriptor} object.
	 * 
	 * @param aProxy The proxy in question.
	 * @param aType The type of the proxy in question.
	 * @param aInstanceId The instance ID assigned to the proxy in question.
	 */
	public ProxyDescriptor( Object aProxy, Class<?> aType, String aInstanceId ) {
		super( aType, aInstanceId );
		_proxy = aProxy;
	}

	/**
	 * Creates a new {@link ProxyDescriptor} object.
	 * 
	 * @param aClassDescriptor The descriptor describing the class of the proxy
	 *        in question.
	 * @param aProxy The proxy in question.
	 */
	public ProxyDescriptor( ClassDescriptor aClassDescriptor, Object aProxy ) {
		super( aClassDescriptor );
		_proxy = aProxy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <P> P getProxy() {
		return (P) _proxy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return Execution.toString( "Instance ID = <" + getInstanceId() + ">; proxy = " + _proxy.toString(), super.toString() );
	}
}
