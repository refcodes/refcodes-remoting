// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import org.refcodes.exception.ExceptionAccessor.ExceptionProperty;

/**
 * The Interface Reply.
 */
public interface Reply extends Session, ExceptionProperty<Throwable> {

	/**
	 * Returns the return value of a method call. To test first if the reply
	 * does not contain a Throwable object (exception occurred!) use the method
	 * isThrowable().
	 * 
	 * @return The return value of the method call (if any),
	 */
	Object getReturnValue();

	/**
	 * Returns true if the reply carries an exception which occurred when
	 * executing the method call.
	 * 
	 * @return True if this object contains an exception (Throwable object)
	 *         instead of a return value.
	 */
	boolean isException();

	/**
	 * Returns true if the reply carries a valid return value.
	 * 
	 * @return True if this object contains valid return value.
	 */
	boolean isReturnValue();

	/**
	 * Sets the return value.
	 *
	 * @param aValue Description is currently not available!
	 */
	void setReturnValue( Object aValue );

	/**
	 * Sets a reply. The results (getThrowable(), getReturnValue()) from the
	 * provided reply are taken and set inside this instance. If this instance
	 * did not contain a reply before, then the method hasReply will return true
	 * from after this method call.
	 * 
	 * @param aReply An object of type BlueprintMethodReply from which the
	 *        replied values (throwable or return value) will be taken.
	 */
	void setReply( Reply aReply );

	/**
	 * {@inheritDoc}
	 */
	@Override
	void setException( Throwable aException );

	/**
	 * Returns true if this reply is not empty and waiting to be feeded with a
	 * reply. Important for an object waiting for a reply - when method
	 * setMethodReply() has been called, then thi method returns true, or if an
	 * instance has been created already containing a repy (not being empty).
	 * 
	 * @return True if an instance of this interface (which did not contain a
	 *         valid reply) has been feeded with a reply.
	 */
	boolean hasReply();
}
