// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;

import org.refcodes.runtime.Execution;

/**
 * The {@link SubjectDescriptor} is created by the {@link RemoteServer}. It
 * describes the subject to be published. More information such as security
 * information or the location where to publish the object may be implemented as
 * well in extensions of this interface.
 */
class SubjectDescriptor implements SubjectAccessor, Serializable {

	private static final long serialVersionUID = 1L;

	private final Object _subject;

	/**
	 * Creates a new {@link SubjectDescriptor} instance.
	 * 
	 * @param aSubject The subject in question.
	 */
	public SubjectDescriptor( Object aSubject ) {
		_subject = aSubject;
	}

	/**
	 * Gets the subject.
	 *
	 * @param <S> the generic type
	 * 
	 * @return the subject
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <S> S getSubject() {
		return (S) _subject;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return Execution.toString( "Subject = " + _subject, super.toString() );
	}
}
