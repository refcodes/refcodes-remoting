// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * Provides an accessor for a {@link ClassDescriptor} property.
 */
public interface InstanceDescriptorAccessor {

	/**
	 * Retrieves the {@link ClassDescriptor} from the {@link ClassDescriptor}
	 * property.
	 * 
	 * @return The {@link ClassDescriptor} stored by the {@link ClassDescriptor}
	 *         property.
	 */
	ClassDescriptor getInstanceDescriptor();

	/**
	 * Provides a mutator for a {@link ClassDescriptor} property.
	 */
	public interface InstanceDescriptorMutator {

		/**
		 * Sets the {@link ClassDescriptor} for the {@link ClassDescriptor}
		 * property.
		 * 
		 * @param aInstanceDescriptor The {@link ClassDescriptor} to be stored
		 *        by the {@link ClassDescriptor} property.
		 */
		void setInstanceDescriptor( ClassDescriptor aInstanceDescriptor );
	}

	/**
	 * Provides a builder method for the according property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface InstanceDescriptorBuilder<B extends InstanceDescriptorBuilder<B>> {

		/**
		 * Sets the value for the {@link ClassDescriptor} property.
		 * 
		 * @param aInstanceDescriptor The value to be stored by the
		 *        {@link ClassDescriptor} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withInstanceDescriptor( ClassDescriptor aInstanceDescriptor );
	}

	/**
	 * Provides a {@link ClassDescriptor} property.
	 */
	public interface InstanceDescriptorProperty extends InstanceDescriptorAccessor, InstanceDescriptorMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ClassDescriptor}
		 * (setter) as of {@link #setInstanceDescriptor(ClassDescriptor)} and
		 * returns the very same value (getter).
		 * 
		 * @param aInstanceDescriptor The {@link ClassDescriptor} to set (via
		 *        {@link #setInstanceDescriptor(ClassDescriptor)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ClassDescriptor letInstanceDescriptor( ClassDescriptor aInstanceDescriptor ) {
			setInstanceDescriptor( aInstanceDescriptor );
			return aInstanceDescriptor;
		}
	}
}
