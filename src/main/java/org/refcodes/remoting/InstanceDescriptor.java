// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;

import org.refcodes.mixin.Resetable;

/**
 * The Class InstanceDescriptor.
 */
class InstanceDescriptor implements InstanceId, Resetable, Serializable {

	private static final long serialVersionUID = 1L;

	private String _instanceId;

	/**
	 * Creates a new {@link InstanceId} instance.
	 */
	public InstanceDescriptor() {}

	/**
	 * Creates a new {@link InstanceId} instance.
	 * 
	 * @param aInstanceId The instance ID (of the subject) in question.
	 */
	public InstanceDescriptor( String aInstanceId ) {
		_instanceId = aInstanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInstanceId() {
		return _instanceId;
	}

	/**
	 * Sets the instance id.
	 *
	 * @param aInstanceId the new instance id
	 */
	protected void setInstanceId( String aInstanceId ) {
		_instanceId = aInstanceId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_instanceId = null;
	}
}
