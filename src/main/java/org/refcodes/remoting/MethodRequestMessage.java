// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.lang.reflect.Method;

/**
 * The {@link MethodRequestMessage} describes a method call.
 */
class MethodRequestMessage extends MethodRequestDescriptor implements ClientMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new {@link MethodRequestMessage} instance.
	 */
	public MethodRequestMessage() {}

	/**
	 * Creates a new {@link MethodRequestMessage} instance.
	 * 
	 * @param aMethodRequestDescriptor The {@link MethodRequest} in question.
	 */
	public MethodRequestMessage( MethodRequest aMethodRequestDescriptor ) {
		super( aMethodRequestDescriptor );
	}

	/**
	 * Creates a new {@link MethodRequestMessage} instance.
	 * 
	 * @param aMethod The method in question.
	 * @param aArguments The arguments to be used for invoking the method.
	 * @param aInstanceId The instance ID of the request.
	 * @param aSessionId The session ID for this request.
	 */
	public MethodRequestMessage( Method aMethod, Object[] aArguments, String aInstanceId, String aSessionId ) {
		super( aMethod, aArguments, aInstanceId, aSessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setArgumentArray( Object[] arguments ) {
		super.setArgumentArray( arguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMethodName( String name ) {
		super.setMethodName( name );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setParameterTypes( Class<?>[] parameterTyps ) {
		super.setParameterTypes( parameterTyps );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSessionId( String sessionId ) {
		super.setSessionId( sessionId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMethodRequestDescriptor( MethodRequest methodRequestDescriptor ) {
		super.setMethodRequestDescriptor( methodRequestDescriptor );
	}
}
