// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.remoting;

/**
 * Provides an accessor for a subject property.
 */
public interface SubjectAccessor {

	/**
	 * Retrieves the subject from the subject property.
	 * 
	 * @param <S> The expected type of the subject.
	 * 
	 * @return The subject stored by the subject property.
	 */
	<S> S getSubject();

	/**
	 * Provides a mutator for a subject property.
	 */
	public interface SubjectMutator {

		/**
		 * Sets the subject for the subject property.
		 * 
		 * @param <S> The expected type of the subject.
		 * @param aSubject The subject to be stored by the subject property.
		 */
		<S> void setSubject( S aSubject );
	}

	/**
	 * Provides a builder method for the according property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SubjectBuilder<B extends SubjectBuilder<B>> {

		/**
		 * Sets the value for the subject property.
		 * 
		 * @param <S> The expected type of the subject.
		 * @param aSubject The value to be stored by the subject property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		<S> B withSubject( S aSubject );
	}

	/**
	 * Provides a subject property.
	 */
	public interface SubjectProperty extends SubjectAccessor, SubjectMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setSubject(Object)} and returns the very same value (getter).
		 * 
		 * @param <S> The expected type of the subject.
		 * @param aSubject The value to set (via {@link #setSubject(Object)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default <S> S letSubject( S aSubject ) {
			setSubject( aSubject );
			return aSubject;
		}
	}
}
