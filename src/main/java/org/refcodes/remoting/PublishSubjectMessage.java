// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The {@link PublishSubjectMessage} describes a publish or subscribe
 * operation..
 */
class PublishSubjectMessage extends ClassDescriptor implements ServerMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}.
	 */
	public PublishSubjectMessage() {}

	/**
	 * Creates a new {@link PublishSubjectMessage} instance.
	 * 
	 * @param aClassDescriptor The {@link ClassDescriptor} describing the
	 *        subject.
	 */
	public PublishSubjectMessage( ClassDescriptor aClassDescriptor ) {
		this( aClassDescriptor.getType(), aClassDescriptor.getInstanceId() );
	}

	/**
	 * Instantiates a new publish subject aMessage impl.
	 *
	 * @param aType the type
	 * @param aInstanceId the instance id
	 */
	public PublishSubjectMessage( Class<?> aType, String aInstanceId ) {
		super( aType, aInstanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setClass( Class<?> type ) {
		super.setClass( type );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setClassDescriptor( ClassDescriptor classDescriptor ) {
		super.setClassDescriptor( classDescriptor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}
}
