// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

/**
 * The {@link SignOffProxyMessage} describes the operation of signing off a
 * proxy..
 */
class SignOffProxyMessage extends InstanceDescriptor implements ClientMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new {@link SignOffProxyMessage} instance.
	 */
	public SignOffProxyMessage() {}

	/**
	 * Creates a new {@link SignOffProxyMessage} instance.
	 * 
	 * @param aInstanceDescriptor The {@link InstanceId} of the proxy to be
	 *        signed-off.
	 */
	public SignOffProxyMessage( InstanceId aInstanceDescriptor ) {
		this( aInstanceDescriptor.getInstanceId() );
	}

	/**
	 * Creates a new {@link SignOffProxyMessage} instance.
	 * 
	 * @param aInstanceId The instance ID of the proxy to be signed-off.
	 */
	public SignOffProxyMessage( String aInstanceId ) {
		super( aInstanceId );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setInstanceId( String instanceId ) {
		super.setInstanceId( instanceId );
	}
}
