// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting;

import java.io.Serializable;

import org.refcodes.component.Component;
import org.refcodes.component.ConnectableComponent.ConnectableAutomaton;
import org.refcodes.component.ConnectionComponent.ConnectionAutomaton;
import org.refcodes.io.DatagramTransceiver;
import org.refcodes.mixin.BusyAccessor;
import org.refcodes.mixin.Clearable;
import org.refcodes.struct.Containable;

/**
 * The {@link Remote} interface defines the basic functionality in common for
 * both the {@link RemoteClient} and the {@link RemoteServer}.
 */
public interface Remote extends Component, Clearable, Containable, ConnectableAutomaton, ConnectionAutomaton<DatagramTransceiver<Serializable>>, BusyAccessor {}
