module org.refcodes.remoting {
	requires org.refcodes.controlflow;
	requires org.refcodes.data;
	requires org.refcodes.generator;
	requires org.refcodes.runtime;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires java.logging;

	exports org.refcodes.remoting;
}
